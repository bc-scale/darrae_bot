terraform {
  required_version = ">= 1.2.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  
  backend "http" {
  }
}

provider "aws" {
  region  = "ap-northeast-2"
}

resource "aws_vpc" "vpc-for-bot" {
    cidr_block = "10.0.0.0/16"
    enable_dns_hostnames = true
    enable_dns_support = true
    tags = {
      Name = "vpc-for-discord-bot"
    }
}

resource "aws_subnet" "public_subnet_1a" {
    vpc_id = aws_vpc.vpc-for-bot.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "ap-northeast-2a"
    map_public_ip_on_launch = true
    tags = {
      Name = "public-subnet-1a"
    }
}

resource "aws_subnet" "public_subnet_1c" {
    vpc_id = aws_vpc.vpc-for-bot.id
    cidr_block = "10.0.3.0/24"
    availability_zone = "ap-northeast-2c"
    map_public_ip_on_launch = true
    tags = {
        Name = "public-subnet-1c"
    }
}

resource "aws_subnet" "private_subnet_1a" {
    vpc_id = aws_vpc.vpc-for-bot.id
    cidr_block = "10.0.2.0/24"
    availability_zone = "ap-northeast-2a"
    tags = {
        Name = "private-subnet-1a"
    }
}

resource "aws_subnet" "private_subnet_1c" {
    vpc_id = aws_vpc.vpc-for-bot.id
    cidr_block = "10.0.4.0/24"
    availability_zone = "ap-northeast-2c"
    tags = {
        Name = "private-subnet-1c"
    }
}

resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.vpc-for-bot.id
    tags = {
        Name = "Internet-Gateway-bot-VPC"
    }
}

resource "aws_eip" "ngw_ip" {
    vpc = true

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_default_route_table" "public_rt" {
    default_route_table_id = aws_vpc.vpc-for-bot.default_route_table_id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
    }

    tags = {
        Name = "public-route-table"
    }
}

resource "aws_route_table_association" "public_rta_a" {
    subnet_id      = aws_subnet.public_subnet_1a.id
    route_table_id = aws_default_route_table.public_rt.id
}

resource "aws_route_table_association" "public_rta_c" {
    subnet_id      = aws_subnet.public_subnet_1c.id
    route_table_id = aws_default_route_table.public_rt.id
}

resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.vpc-for-bot.id

    tags = {
        Name = "private-route-table"
    }
}

resource "aws_route_table_association" "private_rta_a" {
    subnet_id      = aws_subnet.private_subnet_1a.id
    route_table_id = aws_route_table.private_rt.id
}

resource "aws_route_table_association" "private_rta_c" {
    subnet_id      = aws_subnet.private_subnet_1c.id
    route_table_id = aws_route_table.private_rt.id
}


resource "aws_security_group" "bot_sg" {
  name = "discord-bot-sg"
  vpc_id = aws_vpc.vpc-for-bot.id

  ingress = [
    {
        description = "inbound to http"
        from_port = var.http_port
        to_port = var.http_port
        protocol = "tcp"
        security_groups = []
        self = false
        prefix_list_ids = []
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    },
    {
        description = "inbound to https"
        from_port = var.https_port
        to_port = var.https_port
        protocol = "tcp"
        security_groups = []
        self = false
        prefix_list_ids = []
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    },
  ]

  egress {
    description = "outbound from Web server"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
      Name = "discord-bot-security-group"
  }
}