variable "http_port" {
    type = number
    default = 80
}

variable "https_port" {
    type = number
    default = 443
}