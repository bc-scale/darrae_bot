output "vpc_id" {
    value = aws_vpc.vpc-for-bot.id
}

output "public_subnet_ids" {
    value = [aws_subnet.public_subnet_1a.id, aws_subnet.public_subnet_1c.id]
}

output "private_subnet_ids" {
    value = [aws_subnet.private_subnet_1a.id, aws_subnet.private_subnet_1c.id]
}

output "secure_group_id" {
    value = aws_security_group.bot_sg.id
}